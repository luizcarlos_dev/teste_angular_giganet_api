FROM node:stretch

# Create app directory
RUN mkdir -p /usr/src/teste_giga_net_api_container
WORKDIR /usr/src/teste_giga_net_api_container

# Here go all logs
RUN mkdir -p storage/logs/app
RUN chmod 777 storage/logs/app

# Install dependencies
RUN npm install -g pm2
RUN npm install -g babel-cli

# Start service
EXPOSE 80
EXPOSE 433
CMD [ "sh" ]
