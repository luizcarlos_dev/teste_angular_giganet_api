import headerValidate from './_validates/header.validate';
import createValidate from './_validates/create.validate';
import updateValidate from './_validates/update.validate';
import removeValidate from './_validates/remove.validate';
import create from "./create";
import remove from "./remove";
import update from "./update";
import read from "./read";
import auth from '../../service/auth';

export default (route) => {

    route.get('/user', headerValidate, auth.verifyRequest, read);
    route.post('/user', headerValidate, createValidate, create);
    route.put('/user/:_id', headerValidate, updateValidate, auth.verifyRequest, update);
    route.delete('/user/:_id', headerValidate, removeValidate, auth.verifyRequest, remove);

};