import Response from '../../core/Response';
import User from '../../schemas/mongodb/user';

export default (req, res) => {

    User.create(req.body)
        .then(user => {
            Response.send(res, user, Response.CREATED)
        })
        .catch(erro => {
            Response.send(res, erro, Response.INCORRECT_REQUEST)
        })

}