import Response from '../../core/Response';
import User from '../../schemas/mongodb/user';
import Auth from "../../service/auth";

export default (req, res) => {

    const update = {
        ...req.body,
        password: Auth.encodePassword(req.body.password)
    };

    User.findOneAndUpdate({_id: req.params._id}, {$set: update}, {new: true})
        .then(user => {
            Response.send(res, user, Response.OK);
        })
        .catch(err => {
            Response.send(res, err, Response.INCORRECT_REQUEST);
        });

}