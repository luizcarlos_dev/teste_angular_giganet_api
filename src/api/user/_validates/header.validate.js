import Joi from 'joi';

export default (req, res, next) => {
    Joi
        .object({
            where: Joi.any(),
            sort: Joi.any(),
            group: Joi.any(),
            project: Joi.any(),
            offset: Joi.any(),
            limit: Joi.number()
        })
        .validate(req.query, err => {
            if (err)
                return res.status(400).json(
                    {
                        status : false,
                        code   : 400,
                        data   : '' + err.message,
                        message: 'body_validate_error'
                    }
                );

            next();
        });
}