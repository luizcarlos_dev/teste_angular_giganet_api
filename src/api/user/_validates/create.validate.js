import Joi from 'joi';

export default (req, res, next) => {

    Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required(),
        name: Joi.string().required(),
        birthDate: Joi.string().required(),
        sex: Joi.string().required(),
        username: Joi.string().required(),
        favorites: Joi.object().keys(
            {
                albums: Joi.array().items(
                    Joi.object().keys({
                        _id: Joi.string()
                    })
                ),
                tracks: Joi.array().items(
                    Joi.object().keys({
                        _id: Joi.string()
                    })
                )
            }
        )
    })
        .validate(req.body, err => {
            if (err)
                return res.status(400).json(
                    {
                        status : false,
                        code   : 400,
                        data   : '' + err.message,
                        message: 'body_validate_error'
                    }
                );

            next();
        });
}