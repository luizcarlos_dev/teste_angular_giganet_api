import Response from '../../core/Response';
import User from '../../schemas/mongodb/user';

export default (req, res) => {

    const aggregate = [
        {
            $match: req.query.where
        },
        {
            $sort: {
                name: 1,
                username: 1,
                email: 1
            }
        }
    ];

    User
        .aggregate(aggregate)
        .limit(req.query.limit)
        .skip(req.query.offset)
        .then(users => {
            Response.send(res, users, Response.OK)
        })
        .catch(err => {
            Response.send(res, err, Response.INCORRECT_REQUEST)
        });

}