import Response from '../../core/Response';
import User from '../../schemas/mongodb/user';

export default (req, res) => {

    User.findOneAndRemove({_id: req.params._id})
        .then(user => {
            Response.send(res, user, Response.OK);
        })
        .catch(err => {
            Response.send(res, err, Response.INCORRECT_REQUEST);
        });

}