import Response from '../../core/Response';
import Track from '../../schemas/mongodb/track';

export default (req, res) => {

    Track.findOneAndRemove({_id: req.params._id})
        .then(track => {
            Response.send(res, track, Response.OK);
        })
        .catch(err => {

            console.log('err');
            console.log(err);

            Response.send(res, err, Response.INCORRECT_REQUEST);
        });

}