import Response from '../../core/Response';
import Track from '../../schemas/mongodb/track';

export default (req, res) => {

    Track.create(req.body)
        .then(track => {
            Response.send(res, track, Response.CREATED)
        })
        .catch(erro => {
            Response.send(res, erro, Response.INCORRECT_REQUEST)
        })

}