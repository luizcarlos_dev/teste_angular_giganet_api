import Response from '../../core/Response';
import Track from '../../schemas/mongodb/track';

export default (req, res) => {

    const aggregate = [
        {
            $sort: {
                name: 1,
                username: 1,
                email: 1
            }
        }
    ];

    Track
        .aggregate(aggregate)
        .limit(req.query.limit)
        .skip(req.query.offset)
        .then(tracks => {
            Response.send(res, tracks, Response.OK)
        })
        .catch(err => {
            Response.send(res, err, Response.INCORRECT_REQUEST)
        });

}