import Response from '../../core/Response';
import Track from '../../schemas/mongodb/track';

export default (req, res) => {

    Track.findOneAndUpdate({_id: req.params._id}, {$set: req.body}, {new: true})
        .then(track => {
            Response.send(res, track, Response.OK);
        })
        .catch(err => {
            Response.send(res, err, Response.INCORRECT_REQUEST);
        });

}