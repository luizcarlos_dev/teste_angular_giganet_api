import headerValidate from './_validates/header.validate';
import createValidate from './_validates/create.validate';
import auth from './auth';

export default (route) => {

    route.post('/auth', headerValidate, createValidate, auth);

};