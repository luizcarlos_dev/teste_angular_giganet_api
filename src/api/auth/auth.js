import Response from '../../core/Response';
import User from '../../schemas/mongodb/user';
import Auth from '../../service/auth';

const encodePassword = password => {
    return Auth.encodePassword(password);
};

const encodeAuthToken = (_id, email) => {
    return Auth.encodeAuthToken(_id, email);
};

export default (req, res, next) => {

    const query = {
        email: req.body.email,
        password: encodePassword(req.body.password)
    };

    User.findOne(query)
        .then(user => {
            if (user){

                const update = {
                    ...query,
                    token: encodeAuthToken(user._id, query.email)
                };

                User.findOneAndUpdate(query, {$set: update}, {new: true})
                    .then(user => {
                        if (user){
                            Response.send(res, user, Response.OK)
                        }
                        Response.send(res, err, Response.UNAUTHORIZED);
                    })
                    .catch(err => {
                        Response.send(res, err, Response.UNAUTHORIZED);
                    });
            }else{
                Response.send(res, '', Response.UNAUTHORIZED);
            }
        })
        .catch(err => {
            Response.send(res, err, Response.UNAUTHORIZED);
        });

}