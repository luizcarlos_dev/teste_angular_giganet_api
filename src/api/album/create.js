import Response from '../../core/Response';
import Album from '../../schemas/mongodb/album';

export default (req, res) => {

    Album.create(req.body)
        .then(album => {
            Response.send(res, album, Response.CREATED)
        })
        .catch(erro => {
            Response.send(res, erro, Response.INCORRECT_REQUEST)
        })

}