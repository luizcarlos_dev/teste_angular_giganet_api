import Response from '../../core/Response';
import Album from '../../schemas/mongodb/album';

export default (req, res) => {

    Album.findOneAndUpdate({_id: req.params._id}, {$set: req.body}, {new: true})
        .then(album => {
            Response.send(res, album, Response.OK);
        })
        .catch(err => {
            Response.send(res, err, Response.INCORRECT_REQUEST);
        });

}