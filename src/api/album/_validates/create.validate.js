import Joi from 'joi';

export default (req, res, next) => {

    Joi.object({
        name: Joi.string().required(),
        year: Joi.number().required(),
        artist: Joi.string().required(),
        tracks: Joi.array().items(
            Joi.object().keys({
                _id: Joi.string(),
                name: Joi.string()
            })
        )
    })
        .validate(req.body, err => {
            if (err)
                return res.status(400).json(
                    {
                        status : false,
                        code   : 400,
                        data   : '' + err.message,
                        message: 'body_validate_error'
                    }
                );

            next();
        });
}