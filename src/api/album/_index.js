import headerValidate from './_validates/header.validate';
import createValidate from './_validates/create.validate';
import updateValidate from './_validates/update.validate';
import removeValidate from './_validates/remove.validate';
import create from "./create";
import update from "./update";
import read from "./read";
import auth from '../../service/auth';
import remove from "./remove";

export default (route) => {

    route.get('/album', headerValidate, auth.verifyRequest, read);
    route.post('/album', headerValidate, auth.verifyRequest, createValidate, create);
    route.put('/album/:_id', headerValidate, updateValidate, auth.verifyRequest, update);
    route.delete('/album/:_id', headerValidate, removeValidate, auth.verifyRequest, remove);

};