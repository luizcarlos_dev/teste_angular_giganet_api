import Response from '../../core/Response';
import Album from '../../schemas/mongodb/album';

export default (req, res) => {

    const aggregate = [
        {
            $sort: {
                name: 1,
                username: 1,
                email: 1
            }
        }
    ];

    Album
        .aggregate(aggregate)
        .limit(req.query.limit)
        .skip(req.query.offset)
        .then(albums => {
            Response.send(res, albums, Response.OK)
        })
        .catch(err => {
            Response.send(res, err, Response.INCORRECT_REQUEST)
        });

}