import mongoose from 'mongoose';

/**
 * Use this class for all methods that manage databases connections, MySQL, PGSql, MongoDB etc..
 */
export default class Database {

    /**
     *
     * @param databaseConfig
     * @returns {Connection}
     * @private
     */
    _connectInMongoDB(databaseConfig) {
        mongoose.Promise = global.Promise;
        return mongoose.connection.openUri(this._createUri('mongodb', databaseConfig));
    }

    /**
     * Return many URI connections based in SGBD drivers
     * @param driver
     * @param config
     * @returns {string}
     * @private
     */
    _createUri(driver, config) {
        return  config.user.length
            ? `${driver}://${config.user}:${config.pass}@${config.host}:${config.port}/${config.name}`
            : `${driver}://${config.host}:${config.port}/${config.name}`;
    }

    /**
     * Public method to connect mongodb, accept callback for success
     * @param databaseConfig
     * @param success
     */
    connectMongo(databaseConfig, success) {
        this._connectInMongoDB(databaseConfig)
            .then(() => {
                success();
            })
            .catch(err => {
                console.log('[MongoDB Error] \n\n\t' + err.message + '\n\tEXIT\n');
                process.exit(0);
            });
    }
}