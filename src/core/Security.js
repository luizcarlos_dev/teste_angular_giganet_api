import helmet from 'helmet';

export default class Security {

    makeSecure(app) {

        // 1 Day in seconds
        const oneDay = 86400;
        // HTKP Keys
        const hpkpKeys = [
            'Khfmv0ivWiSUD8YmOg=',
            '3qm8YMsINLT2yXS0HxpVJCsCvp0bpgapP7ix=='
        ];

        // Require HTTPS
        app.use(helmet.hsts({
            maxAge: oneDay
        }));

        // Prevent MITM (Man in the middle attack)
        app.use(helmet.hpkp({
            maxAge: oneDay,
            sha256s: hpkpKeys
        }));

        // No referrer
        app.use(helmet.referrerPolicy({
            policy: 'same-origin'
        }));

        // Block IE download
        app.use(helmet.ieNoOpen());

        // Block iframe usage (clickjacking)
        app.use(helmet.frameguard({
            action: 'same-origin'
        }))
    }
}