export default class ApiConfig {

    constructor() {
        this._loadEnvironment();
    }

    _loadEnvironment() {
        this.envname = process.env.NODE_ENV;
        this.env = require('./env/' + this.envname + '.env.js');
    }
}