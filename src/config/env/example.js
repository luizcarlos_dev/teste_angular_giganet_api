module.exports = {
    app: {
        name: 'Teste Giga Net - DEV MODE',
        version: '0.0.1'
    },
    server: {
        secure: false,
        host: '0.0.0.0',
        port: 80,
        cors: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        }
    },
    jwt: {
        jwtSecret: '4ykw@W+U7d@rS!_?Y*EDw@*x@fuD-4Z7&5sWKcHR',
        jwtExpiresIn: '7 days'
    },
    databases: {
        mongodb: {
            host: '172.27.0.1',
            port: 27017,
            user: '',
            pass: '',
            name: 'teste_giga_net?authSource=admin',
            dialect: 'mongodb',
            logging: true
        }
    }
};