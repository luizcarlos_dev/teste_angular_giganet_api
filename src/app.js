import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import ApiConfig from './config/api.conf';
import SSL from './core/SSL';
import https from 'https';
import compression from 'compression';
import Security from './core/Security';
import Cors from './core/Cors';
import Routers from './core/Routers';
import Response from './core/Response';
import Database from './core/Database';
import RequestQuery from './core/RequestQuery';

const security      = new Security();
const config        = new ApiConfig();
const ssl           = new SSL().getCredentials();
const cors          = new Cors();
const routers       = new Routers();
const database      = new Database();
const requestQuery  = new RequestQuery();

let app = express();

// No use logs in test environment!
if (config.envname !== 'test') {
    app.use(morgan(config.envname === 'development'? 'dev' : 'combined'));
}

app.use(bodyParser.json());
app.use(requestQuery.parseQuery);
app.use(compression());

// Security middlewares with helmet
security.makeSecure(app);

/**
 * Use routes in app
 * @private
 */
const _setupRouters = () => {
    routers.syncRouters(app);
    // Case route not exists send a default 404 response
    app.get('*', function(req, res){
        Response.send(res, null, Response.NOT_FOUND, 'route_not_found');
    });
};

/**
 * Console log output
 * @param text
 * @private
 */
const _appLog = (text) => {
    if (config.envname !== 'test') {
        console.log(text)
    }
};

/**
 * Set the HTTP headers for cors and others
 * @private
 */
const _setupCors = () => {
    config.env.server.cors['x-powered-by'] = config.env.app.name;
    cors.setCors(app, config.env.server.cors)
};

/**
 * Set databases properties and connect
 * @private
 */
const _setupDatabase = () => {

    // NoSQL with MongoDB
    database.connectMongo(config.env.databases.mongodb, () => {
        _appLog('[MongoDB]\tConnection Success');
    });

    _setupCors();
    _setupRouters();
};

/**
 * Set properties and connect
 * @private
 */
const _setups = () => {
    _setupCors();
    _setupRouters();
    _setupDatabase();
};

/**
 * After Express listen with success run the setups functions...
 * @private
 */
const _listenSuccess = () => {
    _setups();
    _appLog(`\n${config.env.app.name} on at ${config.env.server.host}:${config.env.server.port}\n`);
    if (ssl.cert && config.env.server.secure) {
        _appLog('[SSL_ON]\tSecure')
    } else {
        _appLog('[SSL_OFF]\tNOT SECURE (!)')
    }
};

const server = config.env.server.secure && ssl.cert ? https.createServer(ssl, app) : app;

server.listen(config.env.server.port, config.env.server.host, _listenSuccess);

export default app;
