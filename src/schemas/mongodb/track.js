import mongoose from 'mongoose';

export default mongoose.model(
    'Track',
    mongoose.Schema(
        {
            name: {
                type: String,
                required: true,
                unique: false
            },
            duration: {
                type: Number,
                required: true,
                unique: false
            }
        }
    )
);