import mongoose from 'mongoose';

export default mongoose.model(
    'Album',
    mongoose.Schema(
        {
            name: {
                type: String,
                required: true,
                unique: false
            },
            year: {
                type: Number,
                required: true,
                unique: false
            },
            artist: {
                type: String,
                required: true,
                unique: false
            },
            tracks:[
                {
                    _id:{
                        type: String,
                        required: false,
                        unique: false
                    },
                    name: {
                        type: String,
                        required: true,
                        unique: false
                    }
                }
            ]
        }
    )
);