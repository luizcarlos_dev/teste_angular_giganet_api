import mongoose from 'mongoose';
import Auth from '../../service/auth';

export default mongoose.model(
    'User',
    mongoose.Schema(
        {
            token: {
                type: String,
                required: false,
                unique: true
            },
            email: {
                type: String,
                required: true,
                unique: true
            },
            password:{
                type: String,
                required: true,
                unique: false
            },
            name: {
                type: String,
                required: true,
                unique: false
            },
            birthDate: {
                type: String,
                required: true,
                unique: false
            },
            sex: {
                type: String,
                required: true,
                unique: false,
                enum: [
                    'm',
                    'f'
                ]
            },
            username: {
                type: String,
                required: true,
                unique: false
            },
            favorites:{
                albums:[
                    {
                        _id: {
                            type: String,
                            required: false,
                            unique: false
                        },
                        name: {
                            type: String,
                            required: true,
                            unique: false
                        },
                        artist: {
                            type: String,
                            required: true,
                            unique: false
                        }
                    }
                ],
                tracks:[
                    {
                        _id: {
                            type: String,
                            required: false,
                            unique: false
                        },
                        name: {
                            type: String,
                            required: true,
                            unique: false
                        },
                        duration: {
                            type: Number,
                            required: true,
                            unique: false
                        }
                    }
                ]
            },
        }
    ).pre('save', function (next) {

        if (this.password.length) {
            this.password = Auth.encodePassword(this.password);
        }

        this.token = Auth.encodeAuthToken(this._id, this.email);

        return next();
    })

);