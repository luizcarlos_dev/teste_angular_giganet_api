import Config from '../config/api.conf';
import jwt from 'jsonwebtoken';
import response from '../core/Response';
import crypto from 'crypto'

class Auth {

    constructor () {
        this.algorithm = 'HS256';
        this.headerType = 'Bearer';
        this.config = new Config().env.jwt;

        // Key version control (time of year, change year by year...) encoded in base 64
        this.version = this._base64encode(new Date(new Date().getFullYear().toString()).getTime().toString());

        // Concat version and base key
        this.privateKey = this.config.jwtSecret + this.version;
    }

    /**
     * Secure encode for passwords with sha256
     * @param text
     */
    encodePassword(text) {
        return crypto.createHash('sha256').update(text).digest("hex");
    }

    /**
     * Compare if password and hash is equal
     * @param password
     * @param hash
     */
    comparePassword(password, hash) {
        return this.encodePassword(password) === hash;
    }

    /**
     * Encode auth token, for logged users
     *
     * NOTE: Signature is a BCRYPT hash of jwtSecret key
     *          this hash validate the token in header
     *
     * @param _id
     * @param email
     * @returns {*}
     */
    encodeAuthToken(_id, email) {
        const payload = {
            _id: _id,
            email: email
        };
        return this.headerType + ' ' + this._encodeToken(payload, this.config.jwtExpiresIn)
    }

    /**
     * Encode payload with JWT (NO USE FOR PASSWORDS)
     * @param payload
     * @param expiresIn
     * @param algorithm
     * @returns {*}
     * @private
     */
    _encodeToken(payload, expiresIn = null, algorithm = this.algorithm) {
        return expiresIn
            ? jwt.sign(payload, this.privateKey,
                {
                    algorithm: algorithm,
                    expiresIn: expiresIn
                })
            : jwt.sign(payload, this.privateKey, {algorithm: algorithm});
    }

    /**
     * Encode base64
     * @param text
     * @returns {string}
     * @private
     */
    _base64encode(text) {
        return Buffer.from(text.toString()).toString('base64');
    }

    /**
     * Express middleware for verify token
     * @param req
     * @param res
     */
    verifyRequest(req, res, next) {

        const auth = new Auth();

        if (!req.headers.authorization)
            return response.send(res, null, response.UNAUTHORIZED, 'authorization_missing');

        auth.verifyAuthorization(req.headers.authorization)
            .then(() => {
                next()
            })
            .catch(() => {
                return response.send(res, null, response.UNAUTHORIZED)
            })
    }


    /**
     * Verify any authorization data
     * @param authorization
     */
    verifyAuthorization(authorization) {
        // Verify pure JWT token
        return new Promise((resolve, reject) => {
            const decodedAuth = this.isValidAuthToken(authorization);

            // Check if data decoded exists
            if (!decodedAuth) {
                reject(Error('no valid authorization found'))
            }

            resolve(decodedAuth)
        })

    }

    /**
     * Verify auth token validate
     * @returns {Boolean}
     * @param authorization
     */
    isValidAuthToken(authorization) {

        // split authorization header, verify valid type and get real token
        const token = this._splitHeaderType(authorization, this.headerType);

        // Try decode send token
        const jwToken = this.decodeToken(token);

        // Remove JWT issueAt and expiresIn information (unnecessary) (-84 Bytes)
        jwToken && jwToken.iat ? Reflect.deleteProperty(jwToken, 'iat') : null;
        jwToken && jwToken.exp ? Reflect.deleteProperty(jwToken, 'exp') : null;

        // Verify if token have a valid fields
        return !jwToken || !jwToken._id ? false : jwToken;
    }

    /**
     * Split "authorization" header and verify if the type before token is accepted
     * @param authorization
     * @param type
     * @returns {*}
     * @private
     */
    _splitHeaderType(authorization, type) {
        // Split the authorization type
        const tokenParts = authorization.split(type + ' ');

        // Not a valid type authorization
        if (tokenParts.length !== 2) {
            return null;
        }

        // Last part (2) is the real token
        return tokenParts.pop();
    }

    /**
     * Decode JWT token and return payload
     * @param token
     * @param algorithm
     * @returns {*}
     */
    decodeToken(token, algorithm = this.algorithm) {
        try {
            return jwt.verify(token, this.privateKey, {algorithm: algorithm});
        } catch (exc) {
            return null;
        }
    }

}

export default new Auth()