# Teste Angular Giganet Api

Api usada para construção do front do teste

[![node](https://img.shields.io/badge/NodeJS-8-green.svg?style=flat-square)]()
[![es6](https://img.shields.io/badge/ES6-Babel-blue.svg?style=flat-square)]()

  
# Clone the repo
```sh
    git clone https://luizcarlos_dev@bitbucket.org/luizcarlos_dev/teste_angular_giganet_api.git
    cd teste_angular_giganet_api
```

# Install packages
```sh
    npm install
    sudo npm install pm2 -g
```

# Adjust the environment
```sh
    cp src/config/env/example.js src/config/env/development.env.js
    cp src/config/env/example.js src/config/env/production.env.js
```

# Replace ssl certificates (if using ssl)

```sh
    cp src/storage/certificates/fullchain-example.crt src/storage/certificates/fullchain.crt
    cp src/storage/certificates/privkey-example.key src/storage/certificates/privkey.key
```

# Build
```sh
    npm run build
```

# Run (local)
```sh
    npm run dev   # (Development mode)
    npm run test  # (Mocha tests)
    npm start     # (Production mode with pm2, build before)
    npm stop      # (Stop Production)
```
  
# Deploy (Docker)
```sh
    sudo npm run deploy   # Pull a base image and start new container
    sudo npm run undeploy # Stop and remove container
```
